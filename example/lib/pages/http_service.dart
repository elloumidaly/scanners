import 'dart:convert';
import 'dart:io';
import 'package:document_scanner_flutter_example/pages/login_page.dart';
import 'package:document_scanner_flutter_example/pages/scan-page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'RESET.dart';
import 'forgot_password_verification_page.dart';
import 'package:shared_preferences/shared_preferences.dart';







class HttpService {
  static final String baseip= "http://192.168.1.13:7000/";
  static final _client = http.Client();

  static final _loginUrl = Uri.parse('$baseip/login');

  static final _registerUrl = Uri.parse('$baseip/register');

  static final _logout = Uri.parse('$baseip/logout');


  static final _forget = Uri.parse('$baseip/forgot');

  static final _sendVerificationCode = Uri.parse('$baseip/verify');




  static login(email, password, context) async {
    final storage = new FlutterSecureStorage();
    http.Response response = await _client.post(_loginUrl, body: {
      "email": email,
      "password": password,
    });

    print(response.body);
    if (response.statusCode == 200) {
      String accessToken = ((json.decode(response.body)['access_token']));
      await storage.write(key: "accessToken", value: accessToken);
      print('******');
      print(storage);

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MyApp()));
    }
    else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode}");
    }


  }


  static register(email, password, context) async {
    final storage = new FlutterSecureStorage();
    http.Response response = await _client.post(_registerUrl, body: {

      "email": email,
      "password": password,

    });
    print(response.body);


    if (response.statusCode == 200) {
      String accessToken = ((json.decode(response.body)['access_token']));
      await storage.write(key: "accessToken", value: accessToken);
      print('******');
      print(storage);

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    }
    else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode}");
    }
  }


  static logout( context) async {
    http.Response response = await _client.post(_logout, body: {

    });

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      if (json[0] == 'GOOD BYE') {
        await EasyLoading.showError(json[0]);
      } else {
        await EasyLoading.showSuccess(json[0]);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }
    } else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode.toString()}");
    }
  }

  static forget(email,context) async {
    final storage = new FlutterSecureStorage();

    http.Response response = await _client.post(_forget, body: {

      "email": email,


    });
    print(response.body);


    if (response.statusCode == 200) {
      String accessToken = ((json.decode(response.body)['access_token']));
      await storage.write(key: "accessToken", value: accessToken);

      print('******');


      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) =>ForgotPasswordVerificationPage ()));
    }
    else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode}");
    }
  }



  static Future<List<dynamic>> csendVerificationCode(code) async {
    final http.Response response;
    var OTP_code_verified;
    final storage = FlutterSecureStorage();
    String? resetToken = await storage.read(key: "resetToken");
    print("resetToken : ");
    print(resetToken);
    try {
      print("try to send");
      response = await http
          .post(
        Uri.parse('http://192.168.1.155:7000/verify'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': "Bearer $resetToken"
        },
        body: jsonEncode(
            <String, int>{'verification_code': code}),
      )
          .timeout(const Duration(seconds: 3));
      print("sent");
    } catch (TimeoutException) {
      return [522,OTP_code_verified];
    }



    if (response.statusCode == 200) {
      OTP_code_verified = ((json.decode(response.body)['OTP_code_verified']));
      print("OTP_code_verified : " );
      print(OTP_code_verified);
    }

    print("status code : ");
    print(response.statusCode);
    return  [response.statusCode,OTP_code_verified];
  }
  static sendVerificationCode(code,context) async {
    final storage = new FlutterSecureStorage();

    String? token = await storage.read(key: "resetToken");
    Map<String, String> headers = { "authorization": "Bearer $token"};
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': "Bearer $token"
    };

    http.Response response = await _client.post(_sendVerificationCode, body: {

      "code": code,



    });
    print(response.body);


    if (response.statusCode == 200) {
      String accessToken = ((json.decode(response.body)['access_token']));
      await storage.write(key: "accessToken", value: accessToken);

      print('******');


      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) =>REST ()));
    }
    else {
      await EasyLoading.showError(
          "Error Code : ${response.statusCode}");
    }
  }
  static Future<int> resetPassword(newPassword) async {
    final http.Response response;
    final storage = FlutterSecureStorage();
    String? resetToken = await storage.read(key: "resetToken");
    print("resetToken : ");
    print(resetToken);
    try {
      print("try to send");
      response = await http
          .post(
        Uri.parse('http://192.168.1.156:5000/reset'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': "Bearer $resetToken"
        },
        body: jsonEncode(
            <String, String>{'new_password': newPassword}),
      )
          .timeout(const Duration(seconds: 100));
      print("sent");
    } catch (TimeoutException) {
      return 522;
    }

    final responseData = json.decode(response.body);
    print(responseData.toString());
    return  response.statusCode;
  }
}
