import 'dart:convert';
import 'dart:math';

import 'package:document_scanner_flutter_example/pages/theme_helper.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() => runApp(MaterialApp(
  home: Picker(),
));

class Picker extends StatelessWidget {



  List<PlatformFile>? files;

  void _uploadFile() async {
    final storage = new FlutterSecureStorage();
    String? token = await storage.read(key: "accessToken");

    Map<String, String> headers = { "authorization": "Bearer $token"};
    final multipartRequest = new http.MultipartRequest('POST', Uri.parse('http://192.168.1.13:5000/classification'));
    multipartRequest.headers["authorization"] = "Bearer $token";
    print("************");
    print("************");
    print("************");
    print('authorization": "Bearer $token');



    multipartRequest.headers.addAll(headers);
    multipartRequest.files.add(await http.MultipartFile.fromPath('file', files!.first.path.toString()));
    var response =await multipartRequest.send();
    print("************");
    print(response);
    var responsed = await http.Response.fromStream(response);
    final responseData = json.decode(responsed.body);


    if (response.statusCode==200) {
      print("SUCCESS");
      print(responseData);

    }
    else {
      print("ERROR");
    }

  }

  void _openFileExplorer() async {
    files = (await FilePicker.platform.pickFiles(
        type: FileType.any,
        allowMultiple: false,
        allowedExtensions: null
    ))!.files;

    print('Loaded file path is : ${files!.first.path}');
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace:Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).accentColor,]
              )
          ),
        ),

        title: Text('File Upload'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [


            Container(
              decoration: ThemeHelper().buttonBoxDecoration(context),
              child: ElevatedButton(
                style: ThemeHelper().buttonStyle(),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.open_in_browser),
                        Text("find File")
                      ],
                    )

                ),
                onPressed: () => _openFileExplorer(),
              ),
            ),
            SizedBox(height: 20,),
            Container(
              decoration: ThemeHelper().buttonBoxDecoration(context),
              child: ElevatedButton(
                style: ThemeHelper().buttonStyle(),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.upload_file),
                        Text("Upload File")
                      ],
                    )

                ),
                onPressed: () => _uploadFile(),
              ),
            ),


          ],
        ),
      ),
    );
  }
}
