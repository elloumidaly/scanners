import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:document_scanner_flutter_example/pages/scan-page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class list extends StatefulWidget {
  const list({Key? key}) : super(key: key);



  @override
  _DashboardState createState() => _DashboardState();
}

Future<bool> deleteProfile(String resume) async {
  var map = new Map<String, dynamic>();
  map['file']=resume;
  final http.Response response = await http.delete(
      Uri.parse("http://192.168.1.13:5000/resumes/delete?file=$resume"),

  );

  if (response.statusCode == 200) {
    return true;

  } else {
    return false;
  }
}
Future <List<Data>> fetchData() async {
  final response = await http.get(Uri.parse('http://192.168.1.13:5000/resumes/list'));
  if (response.statusCode == 200) {
    Map<String, dynamic> map = json.decode(response.body);

    print("**********************");

  /*print(map["5"]);

    List jsonResponse = [map["1"],map["2"],map["3"],map["4"],map["5"],map["6"]];
    print(jsonResponse);*/
    List jsonResponse = [];


    for (var i = 0; i < (map.length); i++) {

      jsonResponse.add( map[i.toString()]);
      print(jsonResponse);
    }

    return jsonResponse.map((data) => new Data.fromJson(data)).toList();

  } else {
    throw Exception('Unexpected error occured!');
  }
}


class Data {
  Data({
    required this.confidenceLevel,
    required this.path,
    required this.profile,
    required this.resume,
    required this.skills,
  });

  double confidenceLevel;
  String path;
  String profile;
  String resume;
  String skills;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    confidenceLevel: json["Confidence_level"].toDouble(),
    path: json["Path"],
    profile: json["Profile"],
    resume: json["Resume"],
    skills: json["Skills"],
  );

  Map<String, dynamic> toJson() => {
    "Confidence_level": confidenceLevel,
    "Path": path,
    "Profile": profile,
    "Resume": resume,
    "Skills": skills,
  };
}
getdata(){

}

class _DashboardState extends State<list> {
  List<File> files = [];
  late Future <List<Data>> futureData;








  @override
  void initState() {
    super.initState();
    futureData = fetchData();
  }



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false ,
      title: 'Liste Profils ',
      home: Scaffold(
        appBar: AppBar(
          flexibleSpace:Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).accentColor,]
                )
            ),
          ),
          title: Text('List Profils'),
        ),
        body: Center(
          child: FutureBuilder <List<Data>>(
            future: futureData,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<dynamic>? data = snapshot.data;
                return
                  ListView.builder(
                      itemCount: data!.length,
                      itemBuilder: (BuildContext context, int index) {



                        return Container(
                          height: 75,
                          color: Colors.white,
                          child:ListTile(
                              leading: Icon(Icons.people),

                              title: Text(data[index].profile),
                              subtitle: Text(data[index].resume),
                              onTap:()=> Navigator.of(context).push(
                                    MaterialPageRoute(
                                     builder: (context) => MyApp(

                              ),
                                ),
                              ),

                              trailing: Wrap(children: [


                                FlatButton(
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            title: Text("Warning"),
                                            content: Text("Are you sure want to delete data profile ${data[index].profile}?"),
                                            actions: <Widget>[
                                              FlatButton(
                                                child: Text("Yes"),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  deleteProfile(data[index].resume).then((isSuccess) {
                                                    if (isSuccess) {

                                                      setState(() {});
                                                      Scaffold.of(this.context)
                                                          .showSnackBar(SnackBar(content: Text("Delete data success")));

                                                    } else {
                                                      Scaffold.of(this.context)
                                                          .showSnackBar(SnackBar(content: Text("Delete data failed")));
                                                    }
                                                  });
                                                },
                                              ),
                                              FlatButton(
                                                child: Text("No"),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                              )
                                            ],
                                          );
                                        });
                                  },
                                  child: Text(
                                    "Delete",
                                    style: TextStyle(color: Colors.red),
                                  ),
                                ),


                          ],)),



                          );
                      }
                  );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              // By default show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}