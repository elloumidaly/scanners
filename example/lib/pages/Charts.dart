import 'package:d_chart/d_chart.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/material.dart';


class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);


  @override
  _DashboardState createState() => _DashboardState();
}


class _DashboardState extends State<Dashboard> {



    Future<http.Response> getResponse(String endpoint) async {
      var url = Uri.parse(
          'http://192.168.1.13:7000/resumes/nb-res-per-profil');
      var response = await http.get(url);
      return response;
    }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: const Text('D\'Chart')),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: DChartPie(
                  data: const [
                    {'domain': 'I.web', 'measure': 4},
                    {'domain': 'I.éléc', 'measure': 5},
                    {'domain': 'I.Test \net Valid', 'measure': 10},
                    {'domain': 'I.Rés \net Télé', 'measure': 4},
                    {'domain': 'I.Embar', 'measure': 8},
                    {'domain': 'non classifié', 'measure': 11}
                  ],
                  fillColor: (pieData, index) {
                    switch (pieData['domain']) {
                      case 'I.web':
                        return Colors.yellowAccent;
                      case 'I.éléc':
                        return Colors.greenAccent;
                      case 'I.Test \net Valid':
                        return Colors.blueAccent;
                      case 'I.Rés \net Télé':
                        return Colors.purpleAccent;
                      case 'I.Embar':
                        return Colors.redAccent;
                      default:
                        return Colors.orangeAccent;
                    }
                  },
                  pieLabel: (pieData, index) {
                    return "${pieData['domain']}:\n${pieData['measure']}";
                  },
                  labelPosition: PieLabelPosition.outside,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  const RotatedBox(quarterTurns: 3, child: Text('Quantity')),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      children: [
                        AspectRatio(
                          aspectRatio: 16 / 9,
                          child: DChartLine(
                            lineColor: (lineData, index, id) {
                              return id == 'Line 1'
                                  ? Colors.blue
                                  : Colors.amber;
                            },
                            data: const [
                              {
                                'id': 'Line 1',
                                'data': [
                                  {'domain': 1, 'measure': 3},
                                  {'domain': 2, 'measure': 3},
                                  {'domain': 3, 'measure': 4},
                                  {'domain': 4, 'measure': 6},
                                  {'domain': 5, 'measure': 0.3},
                                ],
                              },

                            ],
                            includePoints: true,
                          ),
                        ),
                        const Text('Day'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

  }
}

