import 'dart:io';


import 'package:document_scanner_flutter_example/configs/configs.dart';
import 'package:document_scanner_flutter_example/pages/PIcker.dart';
import 'package:document_scanner_flutter_example/pages/theme_helper.dart';
import 'package:document_scanner_flutter_example/pages/widgets/header_widget.dart';
import 'package:flutter/material.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';

import '../document_scanner_flutter.dart';
import 'Charts.dart';
import 'RESET.dart';
import 'forgot_password_page.dart';
import 'forgot_password_verification_page.dart';
import 'liste cv.dart';
import 'login_page.dart';
import 'package:d_chart/d_chart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  double  _drawerIconSize = 24;
  double _drawerFontSize = 17;
  PDFDocument? _scannedDocument;
  File? _scannedDocumentFile;
  File? _scannedImage;


  openPdfScanner(BuildContext context) async {
    var doc = await DocumentScannerFlutter.launchForPdf(
      context,
      labelsConfig: {
        ScannerLabelsConfig.ANDROID_NEXT_BUTTON_LABEL: "Next Steps",
        ScannerLabelsConfig.PDF_GALLERY_FILLED_TITLE_SINGLE: "Only 1 Page",
        ScannerLabelsConfig.PDF_GALLERY_FILLED_TITLE_MULTIPLE:
        "Only {PAGES_COUNT} Page"
      },
      //source: ScannerFileSource.CAMERA
    );
    if (doc != null) {
      _scannedDocument = null;
      setState(() {});
      await Future.delayed(Duration(milliseconds: 100));
      _scannedDocumentFile = doc;
      _scannedDocument = await PDFDocument.fromFile(doc);
      setState(() {});
    }
  }

  openImageScanner(BuildContext context) async {
    var image = await DocumentScannerFlutter.launch(context,
        //source: ScannerFileSource.CAMERA,
        labelsConfig: {
          ScannerLabelsConfig.ANDROID_NEXT_BUTTON_LABEL: "Next Step",
          ScannerLabelsConfig.ANDROID_OK_LABEL: "OK"
        });
    if (image != null) {
      _scannedImage = image;
      setState(() {});
    }
  }

  static const primaryColor = Color(0xFF151026);
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,


      home: Scaffold(

        appBar: AppBar( title: Text("APP2SCAN",
          style: TextStyle(color: Colors.white , fontWeight:FontWeight.bold),
        ),
          elevation: 0.5,
          iconTheme: IconThemeData(color: Colors.white),
          flexibleSpace:Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).accentColor,]
                )
            ),
          ),
          actions: [
            Container(
              margin: EdgeInsets.only( top: 16, right: 16,),
              child: Stack(
                children: <Widget>[
                  Icon(Icons.notifications),
                  Positioned(
                    right: 0,
                    child: Container(
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration( color: Colors.red, borderRadius: BorderRadius.circular(6),),
                      constraints: BoxConstraints( minWidth: 12, minHeight: 12, ),
                      child: Text( '5', style: TextStyle(color: Colors.white, fontSize: 8,), textAlign: TextAlign.center,),
                    ),
                  )
                ],
              ),
            )

          ],

        ),
        drawer: Drawer(
          child: Container(
            decoration:BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.0, 1.0],
                    colors: [
                      Theme.of(context).primaryColor.withOpacity(0.2),
                      Theme.of(context).accentColor.withOpacity(0.5),
                    ]
                )
            ) ,
            child: ListView(
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [0.0, 1.0],
                      colors: [ Theme.of(context).primaryColor,Theme.of(context).accentColor,],
                    ),
                  ),
                  child: Container(
                    alignment: Alignment.bottomLeft,
                    child: Text("Profil",
                      style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),

                ListTile(
                  leading: Icon(Icons.bar_chart,size: _drawerIconSize,color: Theme.of(context).accentColor),
                  title: Text('Charts', style: TextStyle(fontSize: _drawerFontSize, color: Theme.of(context).accentColor),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()),);
                  },
                ),
                Divider(color: Theme.of(context).primaryColor, height: 1,),

                ListTile(
                  leading: Icon(Icons.list_alt_rounded,size: _drawerIconSize,color: Theme.of(context).accentColor),
                  title: Text('List CVs', style: TextStyle(fontSize: _drawerFontSize, color: Theme.of(context).accentColor),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => list()),);
                  },
                ),


                Divider(color: Theme.of(context).primaryColor, height: 1,),
                ListTile(
                  leading: Icon(Icons.password_rounded, size: _drawerIconSize,color: Theme.of(context).accentColor,),
                  title: Text('Forgot Password Page',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).accentColor),),
                  onTap: () {
                    Navigator.push( context, MaterialPageRoute(builder: (context) => ForgotPasswordPage()),);
                  },
                ),
                Divider(color: Theme.of(context).primaryColor, height: 1,),
                ListTile(
                  leading: Icon(Icons.miscellaneous_services_sharp, size: _drawerIconSize,color: Theme.of(context).accentColor,),
                  title: Text('Change Password Page',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).accentColor),),
                  onTap: () {
                    Navigator.push( context, MaterialPageRoute(builder: (context) => REST()), );
                  },
                ),
                Divider(color: Theme.of(context).primaryColor, height: 1,),
                ListTile(
                  leading: Icon(Icons.logout_rounded, size: _drawerIconSize,color: Theme.of(context).accentColor,),
                  title: Text('Logout',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).accentColor),),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()),);
                  },
                ),
              ],
            ),
          ),
        ),


        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(height: 100, child: HeaderWidget(100,false,Icons.house_rounded),),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(25, 10, 25, 10),
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(width: 5, color: Colors.white),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Colors.black12, blurRadius: 20, offset: const Offset(5, 5),),
                        ],
                      ),
                      child: Icon(Icons.camera_alt_outlined, size: 80, color: Colors.grey.shade300,),
                    ),
                    SizedBox(height: 20,),
                    Text('SCAN NOW', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),

                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: ThemeHelper().buttonBoxDecoration(context),
                            child: ElevatedButton(
                              style: ThemeHelper().buttonStyle(),
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(Icons.document_scanner_sharp),
                                      Text("SCAN")
                                    ],
                                  )

                              ),
                              onPressed: () => openPdfScanner(context),
                            ),
                          ),
                          SizedBox(height:20,),
                          Container(
                            decoration: ThemeHelper().buttonBoxDecoration(context),
                            child: ElevatedButton(
                              style: ThemeHelper().buttonStyle(),
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(Icons.picture_as_pdf_sharp),
                                      Text("PICK")
                                    ],
                                  )

                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Picker()),);

                              },
                            ),
                          ),
                          SizedBox(height:40,),
                          Padding(
                            padding: const EdgeInsets.all(16),
                            child: AspectRatio(
                              aspectRatio: 16 / 9,
                              child: DChartPie(

                                data: const [
                                  {'domain': 'I.web', 'measure': 4},
                                  {'domain': 'I.éléc', 'measure': 5},
                                  {'domain': 'I.Test \net Valid', 'measure': 10},
                                  {'domain': 'I.Rés \net Télé', 'measure': 4},
                                  {'domain': 'I.Embar', 'measure': 8},
                                  {'domain': 'non classifié', 'measure': 2}
                                ],
                                fillColor: (pieData, index) {
                                  switch (pieData['domain']) {
                                    case 'I.web':
                                      return Colors.yellowAccent;
                                    case 'I.éléc':
                                      return Colors.greenAccent;
                                    case 'I.Test \net Valid':
                                      return Colors.blueAccent;
                                    case 'I.Rés \net Télé':
                                      return Colors.purpleAccent;
                                    case 'I.Embar':
                                      return Colors.redAccent;
                                    default:
                                      return Colors.orangeAccent;
                                  }
                                },
                                pieLabel: (pieData, index) {
                                  return "${pieData['domain']}:\n${pieData['measure']}";
                                },
                                labelPosition: PieLabelPosition.outside,
                              ),
                            ),
                          ),

                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

